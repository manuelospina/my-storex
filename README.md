# Storex

## Notes on the book

This is a development of the application from the book *Phoenix for Rails 
developers* by *Elvio Vicosa*. This file contains notes from the book and my
own annotations.

### Context, models and repos

I like the way phoenix organized the idea of models. But I would prefer to 
separate the schema from the "context". The book groups the schema book and 
the module store under the same directory. I think it is better to create a 
separate directory that holds the schema. It could be called *schema* or
*repo*.

### Context: Store, sales and accounts

Our application is currently composed of two contexts: Store and Sales . The
Store context is responsible for the book catalogue, and the Sales context is
responsible for all finance-related topics.

### Cart and sessions

The Storex application allows customers to manage their shopping carts without
having to sign up for the service. To achieve that, we will use Phoenix sessions to
assign a Cart to a customer. In every request to the application, we will check if a
Cart is present in a session, and if not, we will create a new Cart and associate it
with the user session.

## Phoenix:

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

### Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

