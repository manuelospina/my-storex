# TODO

* Add integration test with hound
* Create Docker
* Update Bcrypt_elixir to 1.0
* Need to handle errors (validate input) in controllers. (See controller test :create)
* After clicking Buy! I get a key :order_id not found Why?
* Add an actual payment method
* User full name should be optional
* move User to a plug: see forward in the router guide
* Order.address is not necessary for my store.
* Check the exercises from the book to see if they are relevant like these ones:
  * Add the support for admins to view the placed orders
  * Add a status field to the order (open, dispatched, canceled) and let admin update those values
  * Add an area where admins can see all users and flag other users as admin
* Separate test in sales to cart, etc and add @xx_unit_test
* Separate Order functions from sales.ex... sales is growing too much
* Create API
* If cart is empty, checkout should be grey-off (or redirect back to cart)
* Move ensure_current_user in checkout controller to a plug (and plug it in routes?) 
  https://robots.thoughtbot.com/testing-elixir-plugs

## In progress


## Done

* **Done!** Logout users! Everyone seems to use Guardian for that:
  https://medium.com/@andreichernykh/phoenix-simple-authentication-authorization-in-step-by-step-tutorial-form-dc93ea350153
* As a developer, I want to have a good unit test coverage, so I can refactor safely
  * Add test for controllers 
    * **DONE** Add test for book controller 
    * **DONE** Add test for user controller
    * **DONE** Add test for cart controller
    * **DONE** Add test for session controller
    * **DONE** Add test for checkout controller
  * Add test for plugs
    * **DONE** admin_only.ex  
    * **DONE** cart.ex  
    * **DONE** current_user.ex  
    * **DONE** items_count.ex
  * Add test for contexts and schemas
    lib/storex/
    ├── accounts
    │   ├── accounts.ex **DONE**
    │   └── user.ex **DONE**
    ├── sales
    │   ├── cart.ex **WON'T DO**
    │   ├── line_item.ex **DONE**
    │   ├── order.ex **DONE**
    │   └── sales.ex **DONE**
    └── store
        ├── book.ex **DONE**
        └── store.ex **DONE**
  * Add test for views and helpers
    ├── price_formatter.ex **DONE**
