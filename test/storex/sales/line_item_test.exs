defmodule Storex.Sales.LineItemTest do
  use Storex.DataCase

  alias Storex.Sales.LineItem

  @moduletag :sales_test_suite

  @valid_attrs %{
    quantity: 2,
  }
 
  test "changeset with valid attributes" do
    changeset = LineItem.changeset(%LineItem{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = LineItem.changeset(%LineItem{}, %{})
    refute changeset.valid?
  end
end
