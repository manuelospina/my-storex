defmodule Storex.Sales.OrderTest do
  use Storex.DataCase

  alias Storex.Sales.Order

  @moduletag :sales_test_suite

  @valid_attrs %{
    address: "2 Mall st. Dokodemo",
  }
 
  test "changeset with valid attributes" do
    changeset = Order.changeset(%Order{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Order.changeset(%Order{}, %{})
    refute changeset.valid?
  end
end
