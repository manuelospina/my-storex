defmodule Storex.SalesTest do
  use Storex.DataCase

  alias Storex.{Sales, Store, Accounts}

  @valid_attrs %{
      title: "My first book",
      description: "My first Story",
      price: "15.90",
      image_url: "product.jpg"
      } 
 
  @book_attrs %{
      title: "My Second book",
      description: "My first Story",
      price: "15.90",
      image_url: "product.jpg"
      } 

  def book_fixture(attrs \\ %{}) do
    {:ok, book} = 
    attrs
    |> Enum.into(@valid_attrs)
    |> Store.create_book()

    book
  end
  
  def cart_fixture() do
    {:ok, cart} = Sales.create_cart()

    cart
  end

  def user_fixture(attrs \\ %{}) do
    default_attrs = %{
      full_name: "John Doe",
      email: "john.doe@test.ltd",
      password: "123456"
    }
    {:ok, user} = attrs
      |> Enum.into(default_attrs)
      |> Accounts.create_user()
    user
  end

  describe "carts" do
    alias Storex.Sales.Cart
    
    test "create_cart/0 creates a car" do
      assert {:ok, %Cart{}} = Sales.create_cart()
    end
    
    test "get_cart!/1 returns a cart" do
      {:ok, cart} = Sales.create_cart()
      assert Sales.get_cart!(cart.id) == cart
    end
    
    test "add_book_to_cart/2 creates or increments a line_item" do
      book = book_fixture()
      {:ok, cart} = Sales.create_cart()
      {:ok, line_item_1} = Sales.add_book_to_cart(book, cart)
      {:ok, line_item_2} = Sales.add_book_to_cart(book, cart)

      assert line_item_1.quantity == 1
      assert line_item_1.book_id == book.id
      assert line_item_1.cart_id == cart.id
      assert line_item_1.id == line_item_2.id
      assert line_item_2.quantity == 2
    end

    test "remove_book_from_cart/2 decrements or deletes a line_item" do
      book = book_fixture()
      {:ok, cart} = Sales.create_cart()
      Sales.add_book_to_cart(book, cart)
      Sales.add_book_to_cart(book, cart)

      {:ok, line_item} = Sales.remove_book_from_cart(book, cart)
      assert line_item.quantity == 1
       
      Sales.remove_book_from_cart(book, cart)
      assert_raise Ecto.NoResultsError, fn ->
        Sales.remove_book_from_cart(book, cart)
      end
    end
    
    test "list_line_items/1 list items that belongs to a cart" do
      book_1 = book_fixture()
      book_2 = book_fixture(@book_attrs)

      {:ok, cart_1} = Sales.create_cart()
      {:ok, cart_2} = Sales.create_cart()

      Sales.add_book_to_cart(book_1, cart_1)
      Sales.add_book_to_cart(book_2, cart_2)

      [line_item_1] = Sales.list_line_items(cart_1)
      assert line_item_1.book == book_1

      [line_item_2] = Sales.list_line_items(cart_2)
      assert line_item_2.book == book_2
    end
    
    test "line_items_quantity_count/1 returns the total quantity of items" do
      book_1 = book_fixture()
      book_2 = book_fixture(@book_attrs)

      {:ok, cart} = Sales.create_cart()

      Sales.add_book_to_cart(book_1, cart)
      Sales.add_book_to_cart(book_1, cart)
      Sales.add_book_to_cart(book_2, cart)

      line_items = Sales.list_line_items(cart)
      assert Sales.line_items_quantity_count(line_items) == 3
    end
    
    test "line_items_total_price/1 returns the total price of items" do
      book_1 = book_fixture()
      book_2 = book_fixture(@book_attrs)

      {:ok, cart} = Sales.create_cart()

      Sales.add_book_to_cart(book_1, cart)
      Sales.add_book_to_cart(book_1, cart)
      Sales.add_book_to_cart(book_2, cart)

      line_items = Sales.list_line_items(cart)
      assert Sales.line_items_total_price(line_items) == Decimal.new("47.70")     
    end
  end
  
  describe "orders" do
    test "new_order/0 returns an empty changeset" do
      assert %Ecto.Changeset{} = Sales.new_order()
    end

    test "process_order/3 creates an order" do
      user = user_fixture()
      cart = cart_fixture()
      book = book_fixture()

      Sales.add_book_to_cart(book, cart)
      Sales.add_book_to_cart(book, cart)

      {:ok, order} = Sales.process_order(user, cart, %{address: "Test Street, 25"})
      [line_item] = order.line_items

      assert order.user_id == user.id
      assert order.address == "Test Street, 25"
      assert line_item.order_id == order.id
      assert line_item.book_id == book.id
      assert line_item.quantity == 2
    end       
  end
end
