defmodule Storex.StoreTest do
  use Storex.DataCase
  alias Storex.Store.Book
  alias Storex.Store

  @moduletag :book_test_suite

  @valid_attrs %{
      title: "My first book",
      description: "My first Story",
      price: "15.90",
      } 
 
  def book_fixture(attrs \\ %{}) do
    {:ok, book} = 
    attrs
    |> Enum.into(@valid_attrs)
    |> Store.create_book()

    book
  end

  describe "store" do
    test "list_books/0 returns all books" do
      book = book_fixture()
      assert Store.list_books == [book]
    end

    test "get_book/1 return a single book" do
      book = book_fixture()
      assert Store.get_book(book.id) == book
    end

    test "create_book/1 with valid data creates a book" do
      assert {:ok, %Book{} = book} = Store.create_book(@valid_attrs)
      assert %Book{} = book
      assert book.title == "My first book"
    end   

    test "update_book/2 with valid data update a book" do
      book = book_fixture()
      assert Store.get_book(book.id) == book
      assert {:ok, %Book{} = book} = Store.update_book(book, 
        %{@valid_attrs | title: "My Second book"})
      assert book.title == "My Second book"
    end

    test "delete_book/1 with valid data delete a book" do
      book = book_fixture()
      assert Store.get_book(book.id) == book
      assert {:ok, %Book{} = book} = Store.delete_book(book)
      refute Store.get_book(book.id) == book
    end


  end
end
