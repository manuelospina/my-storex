defmodule Storex.Store.BookTest do
  use Storex.DataCase
  alias Storex.Store.Book

  @moduletag :book_test_suite

  @valid_attrs %{
      id: "1",
      title: "My first book",
      description: "My first Story",
      price: "15.9",
      image_url: ""
      } 
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Book.changeset(%Book{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Book.changeset(%Book{}, @invalid_attrs)
    refute changeset.valid?
  end
end
