defmodule Storex.Accounts.UserTest do
  use Storex.DataCase
  alias Storex.Accounts
  alias Storex.Accounts.User

  @moduletag :user_test_suite

  @valid_attrs %{
    full_name: "John Doe",
    email: "john.dow@test.ltd",
    password: "123456" 
  }
  @invalid_attrs %{}
 
  def user_fixture(attrs \\ %{}) do
    {:ok, user} = attrs
    |> Enum.into(@valid_attrs)
    |> Accounts.create_user()

    user 
  end

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "check_password/2" do
    user = user_fixture()
    assert {:ok, %User{}} = User.check_password(user, @valid_attrs.password)
    assert {:error, "invalid password"} == User.check_password(user, "Wrong password")
  end
end
