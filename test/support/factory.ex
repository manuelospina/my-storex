defmodule Storex.Factory do
  use ExMachina.Ecto, repo: Storex.Repo

  def user_factory do 
    %Storex.Accounts.User{
      full_name: "John Doe",
      email: sequence(:email, &"email-#{&1}@example.com"),
      password: Comeonin.Bcrypt.add_hash("123456"),
      is_admin: false 
    }
  end  

  def book_factory do 
    %Storex.Store.Book{
      title: "My first book",
      description: "My first Story",
      price: "15.90",
    }
  end

end
