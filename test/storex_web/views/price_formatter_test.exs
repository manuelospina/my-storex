defmodule StorexWeb.Helpers.PriceFormatterTest do
  use StorexWeb.ConnCase, async: true

  alias StorexWeb.Helpers.PriceFormatter

  test "format_price/1 returns a currency value" do
    assert "$10.00" == PriceFormatter.format_price(Decimal.new(10))
  end
  
end

