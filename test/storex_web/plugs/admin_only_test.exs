defmodule StorexWeb.AdminOnlyPlugTest do
  use StorexWeb.ConnCase

  describe "call/2" do
    test "return conn if user is admin" do
      user = insert(:user, is_admin: true)
      conn = session_conn()
        |> put_session(:user_id, user.id) 
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 
      
      assert StorexWeb.Plugs.AdminOnly.call(conn, %{}) == conn
    end

    test "send forbidden response" do
      conn = session_conn()
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 
        |> StorexWeb.Plugs.AdminOnly.call(%{})

      assert conn.status == 403       
    end
  end
end
