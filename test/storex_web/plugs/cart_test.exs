defmodule StorexWeb.CartPlugTest do
  use StorexWeb.ConnCase

  alias Storex.Sales

  @moduletag :cart_test_suite

  describe "call/2" do
    test "cart is assign to the session and cart_id is added" do
      conn = session_conn() |> StorexWeb.Plugs.Cart.call(%{}) 
      assert conn.assigns[:cart]
      assert get_session(conn, :cart_id)
    end

    test "cart is assign to session" do
      {:ok, cart} = Sales.create_cart() 
      conn = session_conn() 
        |> put_session(:cart_id, cart.id) 
        |> StorexWeb.Plugs.Cart.call(%{}) 
      assert conn.assigns[:cart] == cart
      assert get_session(conn, :cart_id) == cart.id
    end
  end

  describe "get/1" do
    test "returns a cart struct" do
      {:ok, cart} = Sales.create_cart() 
      conn = session_conn() 
        |> put_session(:cart_id, cart.id) 
        |> StorexWeb.Plugs.Cart.call(%{}) 

      assert %Storex.Sales.Cart{} = StorexWeb.Plugs.Cart.get(conn) 
      assert StorexWeb.Plugs.Cart.get(conn) == cart
    end
  end

  describe "forget/1" do
    test "delete cart_id from session" do
      {:ok, cart} = Sales.create_cart() 
      conn = session_conn() 
        |> put_session(:cart_id, cart.id) 
        |> StorexWeb.Plugs.Cart.call(%{}) 

      conn = StorexWeb.Plugs.Cart.forget(conn) 
      refute get_session(conn, :cart_id)

      # Should this be happening? 
      # It deletes the session but not the assign. So we will get it with Cart.get/1?
      #  refute conn.assigns[:cart] => Expected false or nil, got %Storex.Sales.Cart{}
      #  refute %Storex.Sales.Cart{} = StorexWeb.Plugs.Cart.get(conn) 
      #
      # Answer from stackoverflow: 
      #  assign works for each request and it's wipe out after the end of the request cycle.
      #  put_session inserts a value in the session and it's available until the session is cleared / expired.
      # https://stackoverflow.com/questions/46502455/what-is-the-difference-between-assign-and-put-session-in-plug-conn-of-the-phoeni
    end
  end

end
