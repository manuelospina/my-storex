defmodule StorexWeb.CurrentUserPlugTest do
  use StorexWeb.ConnCase

  describe "call/2" do
    test "assign user to nil if there is not user" do
      conn = session_conn() 
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 

      assert conn.assigns.current_user == nil
    end

    test "assign user to logged in user" do
      user = insert(:user)
      conn = session_conn()
        |> put_session(:user_id, user.id) 
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 

      assert conn.assigns.current_user.id == user.id
      assert conn.assigns.current_user.email == user.email
    end
  end

  describe "set/2" do
    test "put session, assign user and is_admin" do
      user = insert(:user)
      conn = session_conn() 
        |> StorexWeb.Plugs.CurrentUser.set(user)

      assert conn.assigns.current_user.id == user.id
      assert conn.assigns.current_user.email == user.email
      assert conn.assigns.is_admin == false
      assert get_session(conn, :user_id) == user.id
    end
  end


  describe "get/1" do
    test "returns assigned current user" do
      user = insert(:user)
      conn = session_conn()
        |> put_session(:user_id, user.id) 
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 

      returned_user = StorexWeb.Plugs.CurrentUser.get(conn) 
      assert returned_user.id == user.id
      assert returned_user.email == user.email
    end
  end

  describe "forget/1" do
    test "Delete assigned current user" do
      user = insert(:user)
      conn = session_conn()
        |> put_session(:user_id, user.id) 
        |> StorexWeb.Plugs.CurrentUser.call(%{}) 

      assert get_session(conn, :user_id)
      
      conn = StorexWeb.Plugs.CurrentUser.forget(conn) 
      refute get_session(conn, :user_id)
    end
  end
end
