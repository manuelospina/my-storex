defmodule StorexWeb.ItemCountPlugTest do
  use StorexWeb.ConnCase

  alias Storex.Sales

  describe "get/1" do
    test "returns number of items" do
      conn = session_conn() 
        |> StorexWeb.Plugs.Cart.call(%{}) 
        |> StorexWeb.Plugs.ItemsCount.call(%{}) 

      assert 0 = StorexWeb.Plugs.ItemsCount.get(conn) 

      book = insert(:book)
      {:ok, cart} = Sales.create_cart()
      Sales.add_book_to_cart(book, cart)
      Sales.add_book_to_cart(book, cart)

      assert 2 = session_conn() 
        |> put_session(:cart_id, cart.id)
        |> assign(:cart, cart) 
        |> StorexWeb.Plugs.ItemsCount.call(%{}) 
        |> StorexWeb.Plugs.ItemsCount.get() 
    end
  end


end
