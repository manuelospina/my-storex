defmodule StorexWeb.CartControllerTest do
  use StorexWeb.ConnCase

  @moduletag :cart_test_suite

  describe "GET /carts" do
    test "Return 200 status", %{conn: conn} do
      assert %{status: 200} = get(conn, cart_path(conn, :show))
    end
  end

  describe "POST /carts (:create)" do
    test "Return 302(redirect) status if user is admin", %{conn: conn} do
      book = insert(:book)
      assert %{status: 302} = post(conn, cart_path(conn, :create), %{book_id: book.id})
    end

    test "redirect to cart when passing valid attributes", %{conn: conn} do
      book = insert(:book)
      conn = post(conn, cart_path(conn, :create), %{book_id: book.id})
      assert redirected_to(conn, 302) == cart_path(conn, :show)    
    end

    @tag :skip
    test "Return ?? status when passing invalid attributes", %{conn: conn} do
      _ = post(conn, cart_path(conn, :create), %{book_id: nil})
      # Define what to do 
    end
  end

  describe "DELETE /carts" do
    test "Return 302 status when passing valid attributes", %{conn: conn} do
      book = insert(:book)
      conn = post(conn, cart_path(conn, :create), %{book_id: book.id})
      assert %{status: 302} = delete(conn, cart_path(conn, :delete), %{book_id: book.id})
    end

    test "redirect to cart when passing valid attributes", %{conn: conn} do
      book = insert(:book)
      conn = post(conn, cart_path(conn, :create), %{book_id: book.id})
      conn = delete(conn, cart_path(conn, :delete), %{book_id: book.id})
      assert redirected_to(conn, 302) == cart_path(conn, :show)    
    end

    @tag :skip
    test "Return ?? status when passing invalid attributes", %{conn: conn} do
      _ = delete(conn, cart_path(conn, :delete), %{book_id: nil})
      # Define what to do 
   end
  end
end  
  

