defmodule StorexWeb.BookControllerTest do
  use StorexWeb.ConnCase
  alias Storex.Store
  alias Storex.Accounts

  alias StorexWeb.Plugs.CurrentUser

  @moduletag :book_test_suite

  @valid_attrs %{
      title: "My first book",
      description: "My first Story",
      price: "15.90",
      } 
 
  def book_fixture(attrs \\ %{}) do
    {:ok, book} = 
    attrs
    |> Enum.into(@valid_attrs)
    |> Store.create_book()

    book
  end

  @user_attrs %{
    full_name: "John Doe",
    email: "john.dow@test.ltd",
    password: "123456" 
  }

  def user_fixture(attrs \\ %{}) do
    {:ok, user} = attrs
    |> Enum.into(@user_attrs)
    |> Accounts.create_user()

    user 
  end

  describe "GET /" do
    test "Return 200 status", %{conn: conn} do
      assert %{status: 200} = get(conn, book_path(conn, :index))
    end
  end

  describe "GET /books/:id" do
    test "Return 200 status", %{conn: conn} do
      book = book_fixture()
      assert %{status: 200} = get(conn, book_path(conn, :show, book.id))
    end
  end
  
  describe "GET /books/new" do
    test "Return 403 status if user is not admin", %{conn: conn} do
      # When user is not authenticated
      assert %{status: 403} = get(conn, book_path(conn, :new))

      # When user is authenticated but it is not admin.
      user = insert(:user)
      assert %{status: 403} = 
        session_conn()
        |> CurrentUser.set(user)
        |> get(book_path(conn, :new))
    end

    test "Return 200 status if user is admin", %{conn: conn} do
      {:ok, user} = user_fixture()
        |> Accounts.mark_as_admin()
      assert %{status: 200} = 
        session_conn()
        |> CurrentUser.set(user)
        |> get(book_path(conn, :new))
    end
  end

  describe "POST /books (:create)" do
     test "Return 403 status if user is not admin", %{conn: conn} do
      # When user is not authenticated
      assert %{status: 403} = post(conn, book_path(conn, :create), @valid_attrs)

      # When user is authenticated but it is not admin.
      user = insert(:user)
      assert %{status: 403} = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(book_path(conn, :create), @valid_attrs)
    end

    test "Return 302(redirect) status if user is admin", %{conn: conn} do
      {:ok, user} = user_fixture()
        |> Accounts.mark_as_admin()
      assert %{status: 302} = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(book_path(conn, :create), %{"book" => @valid_attrs})
    end
  end


  describe "GET /books/:id/edit" do
     test "Return 403 status if user is not admin", %{conn: conn} do
      book = book_fixture()

      # When user is not authenticated
      assert %{status: 403} = get(conn, book_path(conn, :edit, book.id))

      # When user is authenticated but it is not admin.
      user = insert(:user)
      assert %{status: 403} = 
        user_session_conn(user)
        |> get(book_path(conn, :edit, book.id))
    end

    test "Return 200 status if user is admin", %{conn: conn} do
      book = insert(:book)
      {:ok, user} = insert(:user)
        |> Accounts.mark_as_admin()
      assert %{status: 200} = 
        user_session_conn(user)
        |> get(book_path(conn, :edit, book.id))
    end
 end

  describe "PUT/PATCH /books/:id (:update)" do
    test "Return 403 status if user is not admin", %{conn: conn} do
      book = book_fixture()

      # When user is not authenticated
      assert %{status: 403} = patch(conn, book_path(conn, :update, book.id), %{"book" => 
          %{
          title: "My updated book",
          description: "My updated Story",
          price: "15.90",
          }})

      # When user is authenticated but it is not admin.
      user = insert(:user)
      assert %{status: 403} = 
        user_session_conn(user)
        |> patch(book_path(conn, :update, book.id), %{"book" => 
          %{
          title: "My updated book",
          description: "My updated Story",
          price: "15.90",
          }})

    end

    test "Return 302 status if user is admin", %{conn: conn} do
      book = insert(:book)
      {:ok, user} = insert(:user)
        |> Accounts.mark_as_admin()
      assert %{status: 302} = 
        user_session_conn(user)
         |> patch(book_path(conn, :update, book.id), %{"book" => 
          %{
          title: "My updated book",
          description: "My updated Story",
          price: "15.90",
          }})
    end
  end

  describe "DELETE /books/:id delete" do
    test "Return 403 status if user is not admin", %{conn: conn} do
      book = book_fixture()

      # When user is not authenticated
      assert %{status: 403} = delete(conn, book_path(conn, :delete, book.id))

      # When user is authenticated but it is not admin.
      user = insert(:user)
      assert %{status: 403} = 
        user_session_conn(user)
        |> delete(book_path(conn, :delete, book.id))
    end

    test "Return 302 status if user is admin", %{conn: conn} do
      book = insert(:book)
      {:ok, user} = insert(:user)
        |> Accounts.mark_as_admin()
      assert %{status: 302} = 
        user_session_conn(user)
         |> delete(book_path(conn, :delete, book.id))
    end
  end
end
