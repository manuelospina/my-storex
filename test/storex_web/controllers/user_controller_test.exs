defmodule StorexWeb.UserControllerTest do
  use StorexWeb.ConnCase

  alias StorexWeb.Plugs.CurrentUser

  @moduletag :user_test_suite

  @valid_attrs %{
    full_name: "John Doe",
    email: "john.dow@test.ltd",
    password: "123456" 
  }

  describe "GET /users/new" do
    test "Return 200 status", %{conn: conn} do
      assert %{status: 200} = get(conn, user_path(conn, :new))
    end
  end

  describe "POST /users (:create)" do
    test "Return 302(redirect) status if attributes are valid", %{conn: conn} do
      assert %{status: 302} = post(conn, user_path(conn, :create), %{"user" => @valid_attrs})
    end

    test "redirect to cart when passing valid attributes", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), %{"user" => @valid_attrs})
      assert redirected_to(conn, 302) == "/carts"
    end

    test "render new when passing invalid attributes", %{conn: conn} do
      %{status: 200, request_path: "/users"} = post(conn, user_path(conn, :create), %{"user" => %{@valid_attrs | password: nil}})
    end
  end
end
