defmodule StorexWeb.CheckoutControllerTest do
  use StorexWeb.ConnCase

  @moduletag :checkout_test_suite

  describe "GET /checkout/new" do
    test "Return 200 status", %{conn: conn} do
      book = insert(:book)
      user = insert(:user)
      assert %{status: 200} = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(cart_path(conn, :create), %{book_id: book.id})
        |> get(checkout_path(conn, :new))
    end
  end


  describe "POST /checkout (:create)" do
    test "Return 302(redirect) status", %{conn: conn} do
      book = insert(:book)
      user = insert(:user)
      assert %{status: 302} = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(cart_path(conn, :create), %{book_id: book.id})
        |> post(checkout_path(conn, :create), %{"order" => %{address: "102 Highgrove st."}})
    end

    test "Redirect to index", %{conn: conn} do
      book = insert(:book)
      user = insert(:user)
      conn = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(cart_path(conn, :create), %{book_id: book.id})
        |> post(checkout_path(conn, :create), %{"order" => %{address: "102 Highgrove st."}})
      assert redirected_to(conn, 302) == "/"
    end

    test "Return 403 status if user is not admin", %{conn: conn} do 
      book = insert(:book)
      user = insert(:user)
      assert %{status: 200} = 
        session_conn()
        |> CurrentUser.set(user)
        |> post(cart_path(conn, :create), %{book_id: book.id})
        |> post(checkout_path(conn, :create), %{"order" => %{address: nil}})
    end

  end
end
